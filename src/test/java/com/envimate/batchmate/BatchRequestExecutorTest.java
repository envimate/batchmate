/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.batchmate;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BatchRequestExecutorTest {

    @Test
    public void execute() {
        final BatchRequestExecutor batchRequestExecutor = BatchRequestExecutor.batchRequestExecutor(10);
        final Map<TaskResultKey, Object> result = batchRequestExecutor.execute(
                new BatchTask<Boolean>() {
                    @Override
                    public TaskResultKey getKey() {
                        return TaskResultKey.fromClassAndKey(Boolean.class, "BoolRequest1");
                    }

                    @Override
                    public Boolean get() {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return Boolean.TRUE;
                    }
                },
                (BatchTask<String>) () -> {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return "done";
                }, new BatchTask<Boolean>() {
                    @Override
                    public TaskResultKey getKey() {
                        return TaskResultKey.fromClassAndKey(Boolean.class, "BoolRequest2");
                    }

                    @Override
                    public Boolean get() {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        return Boolean.FALSE;
                    }
                }

        );

        assertNull(result.get(TaskResultKey.fromClass(Boolean.class)));
        assertEquals(result.get(TaskResultKey.fromClassAndKey(Boolean.class, "BoolRequest1")), Boolean.TRUE);
        assertEquals(result.get(TaskResultKey.fromClassAndKey(Boolean.class, "BoolRequest2")), Boolean.FALSE);
        assertEquals(result.get(TaskResultKey.fromClass(String.class)), "done");
    }

    @Test
    public void executeDuplicateKey() {
        final BatchRequestExecutor batchRequestExecutor = BatchRequestExecutor.batchRequestExecutor(10);
        try {
            batchRequestExecutor.execute(() -> {
                        try {
                            Thread.sleep(1000);
                        } catch (final InterruptedException e) {
                            e.printStackTrace();
                        }
                        return "done";
                    }, () -> {
                        try {
                            Thread.sleep(1000);
                        } catch (final InterruptedException e) {
                            e.printStackTrace();
                        }
                        return "done";
                    }
            );
        }catch(AmbiguousTaskResultKeyException e) {
            assertEquals(e.getMessage(), "Ambiguous key for batch task result. Key: TaskResultKey(resultClass=class java.lang.String, key=). ExistingResult: done. NewResult done");
        }
    }
}