/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.batchmate;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode
@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public final class TaskResultKey {
    private final Class<?> resultClass;
    private final String key;

    static TaskResultKey fromClass(final Class<?> resultClass) {
        return new TaskResultKey(resultClass, "");
    }

    public static TaskResultKey fromClassAndKey(final Class<?> resultClass, final String key) {
        return new TaskResultKey(resultClass, key);
    }
}
