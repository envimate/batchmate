/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.batchmate;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static com.envimate.batchmate.AmbiguousTaskResultKeyException.ambiguousTaskResultKeyException;
import static com.envimate.batchmate.BatchTaskResult.batchTaskResult;
import static java.util.concurrent.CompletableFuture.allOf;
import static java.util.concurrent.CompletableFuture.supplyAsync;

@SuppressWarnings({"CastToConcreteClass", "InstanceofConcreteClass", "ThrowInsideCatchBlockWhichIgnoresCaughtException"})
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
@EqualsAndHashCode
public final class BatchRequestExecutor {
    private final Executor executor;

    public static BatchRequestExecutor batchRequestExecutor(final int numberOfThreads) {
        final Executor executor = Executors.newFixedThreadPool(numberOfThreads);
        return new BatchRequestExecutor(executor);
    }

    public Map<TaskResultKey, Object> execute(final BatchTask<?>... awsRequest) {
        final List<CompletableFuture<?>> completableFutures = Arrays.stream(awsRequest)
                .map((BatchTask<?> batchTask) -> supplyAsync(batchTask)
                        .thenApply(result -> batchTaskResult(result, batchTask.getKey())))
                .collect(Collectors.toList());

        final CompletableFuture<Void> allFutures = allOf(completableFutures.toArray(new CompletableFuture[0]));

        final CompletableFuture<List<?>> allCompletableFuture = allFutures.thenApply(future ->
                completableFutures.stream()
                        .map(CompletableFuture::join)
                        .collect(Collectors.toList()));

        final CompletableFuture<Map<TaskResultKey, Object>> completableFuture = allCompletableFuture.thenApply(result -> {
            final Map<TaskResultKey, Object> resultMap = new HashMap<>(result.size());
            for (final Object o : result) {
                final BatchTaskResult<?> batchTaskResult = (BatchTaskResult<?>) o;
                final TaskResultKey taskResultKey = batchTaskResult.getTaskResultKey();
                final Object newResult = batchTaskResult.getResult();
                final Object alreadyExistingValue = resultMap.put(taskResultKey, newResult);
                if (alreadyExistingValue != null) {
                    throw ambiguousTaskResultKeyException("Ambiguous key for batch task result. " +
                            "Key: %s. ExistingResult: %s. NewResult %s", taskResultKey, alreadyExistingValue, newResult);
                }
            }
            return resultMap;
        });
        try {
            return completableFuture.get();
        } catch (final InterruptedException | ExecutionException e) {
            final Throwable cause = e.getCause();
            if (cause instanceof AmbiguousTaskResultKeyException) {
                throw (AmbiguousTaskResultKeyException) cause;
            }
            throw new UnsupportedOperationException("Something went wrong while executing tasks", e);
        }
    }
}
