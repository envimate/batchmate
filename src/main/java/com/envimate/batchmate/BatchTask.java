/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.batchmate;

import java.util.function.Supplier;

public interface BatchTask<T> extends Supplier<T> {
    default TaskResultKey getKey() {
        return TaskResultKey.fromClass(get().getClass());
    }
}
