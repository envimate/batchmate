/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.batchmate;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@EqualsAndHashCode
final class BatchTaskResult<T> {
    private final T result;
    private final TaskResultKey taskResultKey;

    static <T> BatchTaskResult<T> batchTaskResult(final T result, final TaskResultKey taskResultKey) {
        return new BatchTaskResult<>(result, taskResultKey);
    }

    static <T> BatchTaskResult<T> batchTaskResult(final T result) {
        return new BatchTaskResult<T>(result, TaskResultKey.fromClass(result.getClass()));
    }

    T getResult() {
        return this.result;
    }

    TaskResultKey getTaskResultKey() {
        return this.taskResultKey;
    }
}
