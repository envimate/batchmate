/*
 * Copyright (C) 2017 [Richard Hauswald, Nune Isabekyan] (envimate GmbH - https://envimate.com/)
 */

package com.envimate.batchmate;

final class AmbiguousTaskResultKeyException extends RuntimeException {
    private AmbiguousTaskResultKeyException(final String message) {
        super(message);
    }

    static AmbiguousTaskResultKeyException ambiguousTaskResultKeyException(final String format,
                                                                           final Object... args) {
        return new AmbiguousTaskResultKeyException(String.format(format, args));
    }
}
